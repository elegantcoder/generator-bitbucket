'use strict';
var util = require('util');
var path = require('path');
var fs = require('fs');
var yeoman = require('yeoman-generator');
var Bitbucket = require('./bitbucket');
var bitbucket;

var BitbucketGenerator = module.exports = function BitbucketGenerator() {
  yeoman.generators.Base.apply(this, arguments);

  this.pkg = {};
  this.packageFile = path.join(process.cwd(), 'package.json');
  if (fs.existsSync(this.packageFile)) {
    this.pkg = JSON.parse(this.readFileAsString(this.packageFile));
  }

  this.on('error', function (error) {
    console.error(error);
  }.bind(this));
  console.log(this.yeoman);
};

util.inherits(BitbucketGenerator, yeoman.generators.Base);

BitbucketGenerator.prototype.askForAuth = function askForAuth() {
  var cb = this.async();
  var prompts = [{
    name: 'username',
    message: 'Bitbucket Username',
    default: '',
    validate: function (username) {
      return username.length > 0 && username.length <= 30;
    }
  }, {
    name: 'password',
    message: 'Bitbucket Password',
    type: 'password',
    default: '',
    validate: function (password) {
      return password.length > 0;
    },
  }];

  this.prompt(prompts, function (props) {
    console.log('Login...');
    bitbucket = Bitbucket.createClient({
      username: props.username,
      password: props.password
    });
    bitbucket.getUserAdminPrivilegesTeams(function (err, teams) {
      if (err) {
        console.log('Invalid Username or Password');
        this.askForAuth();
      } else {
        this.teams = teams;
        cb();
      }
    }.bind(this));
  }.bind(this));
};

BitbucketGenerator.prototype.askForOwner = function askForOwner() {
  var cb = this.async();
  var prompts = [{
    name: 'owner',
    type: 'list',
    message: 'Repository Owner? ex) https://bitbucket.org/{owner}',
    choices: this.teams,
    default: this.username
  }];

  this.prompt(prompts, function (props) {
    this.owner = props.owner;
    cb();
  }.bind(this));
};

BitbucketGenerator.prototype.askForProjectName = function askForProjectName() {
  var done = this.async();
  var _this = this;

  var prompts = [{
      name: 'name',
      message: 'Project Name',
      default: function () {
        return _this.pkg.name || __dirname.split(path.sep).pop();
      },
      validate: function (name) {
        return name.length <= 255;
      }
    }];

  this.prompt(prompts, function (props) {
    this.name = props.name;
    done();
  }.bind(this));
};

BitbucketGenerator.prototype.askForOptions = function askForOptions() {
  var cb = this.async();
  var _this = this;

  var prompts = [{
    name: 'description',
    message: 'Description',
    default: function () {
      //from package.json
      return _this.pkg.description || '';
    }
  }, {
    name: 'features',
    message: 'What more would you like?',
    type: 'checkbox',
    choices: [{
      name: 'Private',
      value: 'isPrivate',
      checked: true
    }, {
      name: 'Wiki',
      value: 'hasWiki',
      checked: true
    }, {
      name: 'Issue Tracker',
      value: 'hasIssues',
      checked: true
    }],
  }, {
    name: 'forkingPolicy',
    message: 'choose forking_policy',
    type: 'list',
    choices: ['Allow Forks', 'No Public Forks', 'No Forks'],
    filter: function (choice) {
      return {
        'Allow Forks': 'allow_forks',
        'No Public Forks': 'no_public_forks',
        'No Forks': 'no_forks'
      }[choice];
    },
    default: 'No Public Forks'
  }];

  this.prompt(prompts, function (props) {
    this.description = props.description;
    this.forkingPolicy = props.forkingPolicy;
    // isPrivate, hasWiki, hasIssues
    props.features.forEach(function (item) {
      _this[item] = true;
    });
    cb();
  }.bind(this));
};

BitbucketGenerator.prototype.createRepository = function createRepository() {
  var cb = this.async();
  var _this = this;
  bitbucket.createRepository({
    slug: Bitbucket.slugify(this.name),
    owner: this.owner,
    scm: 'git',
    name: this.name,
    description: this.description,
    is_private: this.isPrivate || false,
    forking_policy: this.forkingPolicy || false,
    has_issues: this.hasIssues || false,
    has_wiki: this.hasWiki
  }, function (err) {
    if (err) {
      switch (err) {
        case Bitbucket.ERROR_UNAUTHORIZED:
          _this.emit('error', 'INVALID_CREDENTIALS');
          break;
        case Bitbucket.ERROR_INVALID_OWNER:
          _this.emit('error', 'INVALID_OWNER');
          break;
        default:
          _this.emit('error', 'UNKNOWN_ERROR');
      }
    } else {
      console.log('Repository created on Bitbucket');
      cb();
    }
  });
};
