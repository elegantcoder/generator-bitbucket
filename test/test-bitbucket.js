/* jshint undef: true, unused: true */
/* global describe, it */
'use strict';
var assert = require('assert');

var username = process.env.BITBUCKET_USERNAME;
var password = process.env.BITBUCKET_PASSWORD;

var Bitbucket = require('../app/bitbucket');

function getRepositoryCreationOptions() {
  return {
    owner: username,
    scm: 'git',
    name: 'test_' + Math.random().toString(36).substring(2, 5),
    slug: 'test_' + Math.random().toString(36).substring(2, 5),
    is_private: true,
    description: 'description',
    forking_policy: 'allow_forks',
    has_issues: true,
    has_wiki: true
  };
}

// Bitbucket.slugify()
describe('Bitbucket Client Test', function () {
  it('slugify test', function () {
    var actual = Bitbucket.slugify('slugifytest-_()!,.asdf');
    // var expected = 'slugifytest-_-.asdf';
    var expected = 'slugifytest-_-asdf';
    assert.equal(actual, expected);

    actual = Bitbucket.slugify('slugifytest,aa');
    expected = 'slugifytest-aa';
    assert.equal(actual, expected);
  });

  describe('AUTH', function () {
    it('should be raised ERROR_UNAUTHORIZED', function (done) {
      var bitbucket = Bitbucket.createClient({
        username: username,
        password: password + '1'
      });
      bitbucket.getUserAdminPrivilegesTeams(function (err) {
        assert.equal(err, Bitbucket.ERROR_UNAUTHORIZED);
        done();
      });
    });

    it('should be returned an array of teams that user has admin privileges', function (done) {
      var bitbucket = Bitbucket.createClient({
        username: username,
        password: password
      });
      bitbucket.getUserAdminPrivilegesTeams(function (err, teams) {
        assert.equal(err, null);
        assert.equal(Object.prototype.toString.call(teams), '[object Array]');
        done();
      });
    });
  });

  describe('get repository', function () {
    it('should be returned object', function () {
      var bitbucket = Bitbucket.createClient({
        username: username,
        password: password
      });

      bitbucket.getRepository({owner: 'elegantcoder', slug: 'generator-bitbucket'}, function (err, response) {
        assert.equal(err, null);
        assert.equal(typeof response, 'object');
      });
    });
  });

  describe('CREATE - GET - DELETE', function () {
    var bitbucket = Bitbucket.createClient({
      username: username,
      password: password
    });
    var creationOptions = getRepositoryCreationOptions();
    it('CREATE success', function (done) {
      bitbucket.createRepository(creationOptions, function (err, response) {
        // assert(err)
        assert.equal(err, null);
        assert(response);
        done();
      });
    });

    it('GET success', function (done) {
      bitbucket.getRepository({
        owner: creationOptions.owner,
        slug: creationOptions.slug
      }, function (err, response) {
        assert.equal(err, null);
        assert(response);
        done();
      });
    });

    it('DELETE success', function (done) {
      var options = {
        owner: creationOptions.owner,
        slug: creationOptions.slug,
      };
      bitbucket.deleteRepository(options, function (err) {
        assert.equal(err, null);
        done();
      });
    });
  });
});
